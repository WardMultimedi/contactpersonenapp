package be.multimedi.contactPersonenApp;

import java.sql.SQLOutput;
import java.util.Scanner;

public class App {
   Scanner keyboard = new Scanner(System.in);
   Telefoonboek telefoonboek = new Telefoonboek();

   public static void main(String[] args) {
      App app = new App();
      app.toevoegenTestData();
      app.startApp();
   }

   public void toevoegenTestData(){
      telefoonboek.toevoegen(new Contactpersoon(
              "Ward", "12311", "abc", "1", "Heverlee"
      ));
      telefoonboek.toevoegen(new Contactpersoon(
              "Frederik", "12322", "abc", "1", "Heverlee"
      ));
      telefoonboek.toevoegen(new Contactpersoon(
              "Rik", "12322", "abc", "1", "Heverlee"
      ));
      telefoonboek.toevoegen(new Contactpersoon(
              "Batman", "12333", "abc", "1", "Heverlee"
      ));

   }

   public void startApp() {
      int keuze;
      do {
         System.out.println("\nHoofd menu Telefoonboek:");
         System.out.println("1. Overzicht");
         System.out.println("2. toevoegen");
         System.out.println("3. verwijderen");
         System.out.println("0. stoppen");
         System.out.print("Uw keuze: ");
         keuze = keyboard.nextInt();
         switch (keuze) {
            case 1:
               overzicht();
               break;
            case 2:
               toevoegen();
               break;
            case 3:
               verwijderen();
            case 0:
               break;
            default:
               System.err.println("Ongeldige keuze");
         }
      } while (keuze != 0);
   }

   void overzicht() {
      System.out.println(telefoonboek);
   }

   String inputString(String question) {
      String value;
      do {
         System.out.print(question);
         value = keyboard.nextLine();
      } while (value.isBlank());
      return value;
   }

   void toevoegen() {
      System.out.println("\nToevoegen contactpersoon:");

      String naam = inputString("Naam contactpersooon: ");
      String tel = inputString("Tel: ");
      String straat = inputString("Straat: ");
      String bus = inputString("bus: ");
      String gemeente = inputString("gemeente: ");

      Contactpersoon cp = new Contactpersoon(naam, tel, straat, bus, gemeente);
      if (telefoonboek.toevoegen(cp))
         System.out.println("Contactpersoon " + naam + " toegevoegd!");
      else
         System.out.println("Fout bij het toevoegen!");
   }

   void verwijderen() {
      System.out.println("\nVerwijderen contactpersoon:");

      String zoekpatroon = inputString("Naam contactpersoon: ");

      Contactpersoon cp = telefoonboek.zoekContactpersoon(zoekpatroon);

      if (cp == null) {
         System.out.println("Geen contactpersoon gevonden!");
      } else {
         System.out.println(cp);
         String input;
         do {
            System.out.print("Bent u zeker deze te verwijderen? (y/n): ");
            input = keyboard.nextLine();
         } while (input.isBlank() && !input.equals("n") && !input.equals("y"));
         if (input.equals("y")) {
            if (telefoonboek.verwijder(cp))
               System.out.println("Verwijderd!");
            else
               System.out.println("Fout bij het verwijderen!");
         }
      }
   }
}
